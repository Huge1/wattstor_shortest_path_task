install:
	virtualenv .venv && . .venv/bin/activate &&  pip install -r requirements.txt
	
.PHONY: test
test:
	. .venv/bin/activate && python -m pytest
