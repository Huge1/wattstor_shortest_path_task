""" Finds optimal route through the places in a given file
    and prints it to the terminal.
"""


import json
import sys
from geopy.distance import distance as geodistance

def calculate_optimal_route(coords: dict):
    """ Brute-force search to try all posible paths that cycle back
        to the starting place.
        Expected line/item/place format is  "CityName": [49.19564, 16.60895]
    """
    print( geodistance([49.19564, 16.60895], [49.39933, 15.58344]).km )
    return coords.keys()

def find_best_round_trip(distances):
    """ generic function for solving the Travelling Salesperson Problem
        precisely, comparing lengths of all the variations/paths.
        Warning: this will only work in reasonable time for small graphs.
    """
    find_best_round_trip(distances[1:]) + min( geodistance(distances[0],...)
    # Simply modify the solution presented at 
    # https://codereview.stackexchange.com/questions/110221/tsp-brute-force-optimization-in-python
    # or any similar.

if __name__ == '__main__':
    #citiesSequenced = calculate_optimal_route({"Brno": [49.19564, 16.60895]})
    # print( citiesSequenced[0] )
    if(len(sys.argv) <= 1):
        print( "a path to a JSON file with locations must be provided as argument" )
        sys.exit(2)
    # The first argument is expected to be a valid JSON file.
    # @todo error handling for wrong path or a malformed content
    with open(sys.argv[1], 'r') as file_name:
        input_in_memory = json.load(file_name)
    calculate_optimal_route( input_in_memory )
