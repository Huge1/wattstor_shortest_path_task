import json
# import pytest # Run with `python -m pytest` in project dir instead.
from calculate_optimal_route import calculate_optimal_route

def test_shortest_path_with_single_place():
    path = calculate_optimal_route({"Brno": [49.19564, 16.60895]})
    assert list(path)[0] == "Brno"
    assert list(path)[-1] == "Brno"

def test_shortest_path():
    path = calculate_optimal_route( json.loads("""
{"Brno": [49.19564, 16.60895],
 "Jihlava": [49.39933, 15.58344],
 "Prague": [50.08144, 14.424088],
 "Beroun": [49.964855, 14.07005],
 "Hradec Kralove": [50.20604, 15.83271]
}""" ) )
    assert list(path)[0] == "Brno"
    assert list(path)[-1] == "Brno"